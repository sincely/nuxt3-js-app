module.exports = {
  apps: [
    {
      name: 'nuxt-app',
      port: '8888',
      exec_mode: 'cluster',
      instances: 'max',
      script: './.output/server/index.mjs',
      merge_logs: true, // 设置追加日志而不是新建日志
      log_date_format: 'YYYY-MM-DD HH:mm:ss', // 指定日志文件的时间格式
      ignore_watch: ['node_modules', 'public', 'logs'] // 不用监听的文件
    }
  ]
}
