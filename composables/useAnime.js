import anime from "animejs/lib/anime.es.js"

const EASING = "easeInOutQuad"
export function useTranslateX(
  el,
  from = 0,
  to = 0,
  delay = 0,
  duration = 200,
  opotion
) {
  const instance = ref()
  onMounted(() => {
    instance.value = anime({
      targets: el.value,
      translateX: [from, to],
      scale: [1.2, 1],
      opacity: [0, 1],
      duration,
      delay,
      easing: EASING,
      ...opotion
    })
  })
  return { instance }
}
export function useTranslateY(
  el,
  from = 0,
  to = 0,
  delay = 0,
  duration = 200,
  opotion
) {
  const instance = ref()
  onMounted(() => {
    instance.value = anime({
      targets: el.value,
      translateY: [from, to],
      scale: [1.2, 1],
      opacity: [0, 1],
      duration,
      delay,
      easing: EASING,
      ...opotion
    })
  })
  return { instance }
}

export function useAnime(opotion) {
  const instance = ref()
  onMounted(() => {
    instance.value = anime({
      ...opotion,
      targets: opotion.targets.value
    })
  })
  return { instance }
}
