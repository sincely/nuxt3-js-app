// https://nuxt.com/docs/api/configuration/nuxt-config
// eslint-disable-next-line no-undef
export default defineNuxtConfig({
  devtools: { enabled: true },
  runtimeConfig: {
    public: {
      baseUrl: process.env.NUXT_BASE_URL
    }
  },
  modules: [
    '@nuxt/image',
    'nuxt-icons',
    '@vueuse/nuxt',
    '@unocss/nuxt',
    '@pinia/nuxt',
    'nuxt-icon',
    '@nuxtjs/color-mode',
    '@nuxtjs/seo',
    '@element-plus/nuxt'
  ],

  app: {
    pageTransition: { name: 'blur', mode: 'out-in' } // 页面过渡效果
  },

  css: [
    '@unocss/reset/tailwind.css',
    'element-plus/theme-chalk/dark/css-vars.css',
    '@/assets/scss/main.scss',
    '@/styles/index.scss',
    '@/styles/anime.css'
  ],
  colorMode: {
    classSuffix: ''
  },
  // devServer: {
  //   port: 3000
  // },
  vite: {
    server: {
      hmr: true,
      watch: {
        usePolling: true //set here to enable hot reload
      },
      hmr: {
        clientPort: 3000
      }
    },
    // css: {
    //   preprocessorOptions: {
    //     less: {
    //       modifyVars: {
    //         'primary-color': '#0AA679'
    //       },
    //       javascriptEnabled: true
    //     }
    //   }
    // },
  },
  // routeRules: {
  //   // Don't add any /secret/** URLs to the sitemap.xml
  //   '/admin/_components/**': { robots: false }
  // }
  compatibilityDate: '2024-12-10'
})
